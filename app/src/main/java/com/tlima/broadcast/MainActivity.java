package com.tlima.broadcast;

import android.content.IntentFilter;
import android.hardware.usb.UsbManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    protected TextView textViewWifi;
    protected TextView textViewUsb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewWifi = (TextView) findViewById(R.id.wifi);
        textViewUsb = (TextView) findViewById(R.id.usb);

        WiFiStateBroadcast wifiStateBroadcast = new WiFiStateBroadcast(textViewWifi);
        UsbStateBroadcast usbStateBroadcast = new UsbStateBroadcast(textViewUsb);

        IntentFilter wifiIntentFilter = new IntentFilter();
        wifiIntentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);

        IntentFilter usbIntentFilter = new IntentFilter();
        usbIntentFilter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);

        registerReceiver(wifiStateBroadcast, wifiIntentFilter);
        registerReceiver(usbStateBroadcast, usbIntentFilter);

    }
}
