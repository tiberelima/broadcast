package com.tlima.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbManager;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class UsbStateBroadcast extends BroadcastReceiver {

    private TextView textView;

    public UsbStateBroadcast(TextView textView) {
        this.textView = textView;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.i("asd", "asd");
        Toast.makeText(context, "USB", Toast.LENGTH_LONG).show();

        if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED)) {
            textView.setText("USB conectado");
        } else {
            textView.setText("USB não conectado");
        }
    }
}
