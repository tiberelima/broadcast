package com.tlima.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.widget.TextView;

public class WiFiStateBroadcast extends BroadcastReceiver {

    private TextView textView;

    public WiFiStateBroadcast(TextView textView) {
        this.textView = textView;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
        if (networkInfo != null) {
            if (networkInfo.isConnected()) {
                textView.setText("Wifi conectado");
            } else {
                textView.setText("Wifi não conectado");
            }
        }
    }
}